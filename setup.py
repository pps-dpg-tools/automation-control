from setuptools import setup

with open("README.org", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(name='automation_controller',
      version='0.1',
      description='Python package to upload data from PPS Automation workflows to the influxdb backend',
      long_description = long_description,
      url='https://gitlab.cern.ch/pps-dpg-tools/automation-control',
      author='Simone Pigazzini, Lukasz Kita',
      author_email='simone.pigazzini@cern.ch, lukasz.radoslaw.kita@cern.ch',
      license='GPLv3',
      packages=[
          'controllers'
      ],
      scripts=[
          'AutomationScript.py',
      ],
      classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: Linux",
      ],
      python_requires=">=3.6",
      install_requires=[
          'influxdb',
      ]
)
