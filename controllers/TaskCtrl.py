import enum
from copy import deepcopy as dcopy
from controllers.credentials import *
from influxdb import InfluxDBClient
from datetime import datetime
import sys

class TaskStatus(enum.Enum):
    """
    Simple class to encode tasks statuses
    """

    initialized = enum.auto()





class TaskControl:
    """
    Update job status and information to the influxdb. 
    Each combination of campaign+era+workflow is called a task.

    The job status is represented by the following table::

       {
           'measurement' : 'task',
           'tags' : {
               'workflow' : 'ECAL offline workflow name',
               'campaign' : 'Campaign to which the job belongs to',
               'dataPeriod'       : 'unique id of a task in the given workflow and campaign'
           },
           'time' : timestamp,
           'fields' : {
               'initialized' : boolean,
               'duringFirstWorker' : boolean,
               'afterFirstWorker' : boolean,
               'duringFirstHarvester' : boolean,
               'afterFirstHarvester' : boolean,
               'duringSecondWorker' : boolean,
               'afterSecondWorker' : boolean,
               'duringSecondHarvester' : boolean,
               'afterSecondHarvester' : boolean,
               'done': boolean
           }        
       }
    """
    
    
    def create_point_data_template(self, TaskStatusClass, workflow, campaign):
        result = {
            'measurement' : None,
            'tags' : {
                'workflow' : str(workflow),
                'campaign' : str(campaign),
                'dataPeriod': 0
            },
            'time' : None,
            'fields' : {
               'initialized' : 0,
               'duringFirstWorker' : 0,
               'afterFirstWorker' : 0,
               'duringFirstHarvester' : 0,
               'afterFirstHarvester' : 0,
               'duringSecondWorker' : 0,
               'afterSecondWorker' : 0,
               'duringSecondHarvester' : 0,
               'afterSecondHarvester' : 0,
               'done': 0
            }
        }   
        return result
    
    
    def __init__(self, workflow=None, campaign=None, TaskStatusClass=TaskStatus):


        ### allow only pre-determined workflows
        if workflow==None:
            sys.exit('[JobCtrl::init] The workflow field is mandatory')

        ### require a campaign
        if campaign==None:
            sys.exit('[JobCtrl::init] The campaign field is mandatory')

        ### create point data template
        self.TaskStatusClass = TaskStatusClass
        self.global_data = self.create_point_data_template(TaskStatusClass, workflow, campaign)
        

        self.db = InfluxDBClient(host=dbhost, port=dbport, username=dbusr, password=dbpwd, ssl=dbssl, database=dbname)
        
    def taskExists(self, data_period):
        """
        Check if specified task exist already in the db.

        :rtype: bool
        """

        exist = len(self.db.query('SELECT * FROM "%s" WHERE "workflow" = \'%s\' AND "campaign" = \'%s\' AND "dataPeriod"=\'%s\' ' % 
                                  ('job', self.global_data['tags']['workflow'], 
                                   self.global_data['tags']['campaign'], data_period)))

        return exist > 0    
        
    def submitTasks(self, data_periods_numbers=[]):
        """
        """

        ### check ids
        if len(data_periods_numbers) < 1:
            sys.exit('[JobCtrl::submittask] tasks numbers list is empty')

        ### check that a task does not already exist for this workflow+campaign combination
        

        ### insert jobs with status set to idle
        data = [dcopy(self.global_data) for _ in range(len(data_periods_numbers))]
        subtime = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
        for i,data_period in enumerate(data_periods_numbers):
            if self.taskExists(data_period):
                print("task: "+str(data_period)+" already exists")
                continue
            data[i]['measurement'] = 'task'
            data[i]['time'] = subtime
            data[i]['tags']['dataPeriod'] = str(data_period)
            data[i]['fields'][self.TaskStatusClass.initialized.name] = 1
        
        return self.db.write_points(data)
     
    
    def setStatus(self, data_period=None, status=None):
        if status is None:
            status = self.TaskStatusClass.initialized

        ### check id
        if data_period==None:
            sys.exit('[taskControl::setStatus] Please specify a vaild task number')

        ### check if job already exist in db (should have been injected by createTask)
        exist = len(self.db.query('SELECT * FROM "%s" WHERE "workflow" = \'%s\' AND "campaign" = \'%s\' AND "dataPeriod" = \'%s\'' % 
                                  ('task', self.global_data['tags']['workflow'], 
                                   self.global_data['tags']['campaign'], str(data_period))))
        if not exist:
            sys.exit('[taskControl::setStatus] task %s not found in %s+%s task. Please submit the task first using JobCtrl::createTask' % 
                     (str(data_period), self.global_data['tags']['workflow'], self.global_data['tags']['campaign']))

        ### check status
        if status.name not in self.TaskStatusClass.__members__.keys():
            sys.exit('[taskControl::setStatus] Specified status %s is not valid. Valid statuses are: \n\t%s' % (status, '\n\t'.join(JobStatus.__members__.keys())))

        data = dcopy(self.global_data)
        data['measurement'] = 'task'
        data['time'] = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
        data['tags']['dataPeriod'] = str(data_period)
        data['fields'][status.name] = 1

        ### check data consistency
        if sum([data['fields'][sts] for sts in self.TaskStatusClass.__members__.keys()]) != 1:
            sys.exit('[taskControl::setStatus] More then one status is being set for the same job (id=%s)' % str(data_period))         
        return self.db.write_points([data])
        
        
    def getTasksWithGivenStatus(self, status):
        tasks_query = self.db.query('SELECT * FROM "task" WHERE "workflow"=\'%s\' AND "campaign"=\'%s\' AND "%s"=1 ORDER BY DESC LIMIT 1' % 
                                   (self.global_data['tags']['workflow'], self.global_data['tags']['campaign'], status.name))

        return tasks_query
        
    
    def getAllTasks(self):
        tasks_query = self.db.query('SELECT *, "dataPeriod", "campaign", "workflow" FROM "task" WHERE "workflow"=\'%s\' AND "campaign"=\'%s\' GROUP BY * ORDER BY DESC LIMIT 1' %
                                    (self.global_data['tags']['workflow'], self.global_data['tags']['campaign']))

        return tasks_query
    