#!/usr/bin/env python3
import os
import sys
import time
import argparse
from io import BytesIO as StringIO
import subprocess 
import controllers as ctrl

class SplitArgs(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values.split(','))

def exist(opts):
    """
    Check if task exist in the db
    """

    jctrl = ctrl.JobCtrl(workflow=opts.workflow, campaign=opts.campaign) 
    exist = jctrl.taskExist(opts.data_period)
    
    print('Task %s+%s %s' % (opts.workflow, opts.campaign, 'exist' if exist else 'does not exist'))
    
    return exist

def submit(opts):
    """
    Inject a new task into the DB
    """

    jctrl = ctrl.JobCtrl(workflow=opts.workflow, campaign=opts.campaign) 
    if opts.ids:
        jctrl.createJobsForTask(ids=[id.strip() for id in opts.ids.split(',')], data_period=opts.data_period)
    elif opts.njobs:
        jctrl.createJobsForTask(ids=[id for id in range(opts.njobs)], data_period=opts.data_period)
    else:
        sys.exit('[ecalsubmit] Please specify one option between --ids and -n')    

    return 0

def jobctrl(opts):
    """
    Update job status in the DB
    """
    
    jctrl = ctrl.JobCtrl(workflow=opts.workflow, campaign=opts.campaign)
    if opts.idle:
        jctrl.idle(opts.id, opts.data_period)
    if opts.running:
        jctrl.running(opts.id, opts.data_period)
    if opts.failed:
        jctrl.failed(opts.id, opts.data_period)
    if opts.done:
        jctrl.done(opts.id, opts.data_period)

    return 0

def status(opts):
    """
    Get current jobs status
    """
    
    jctrl = ctrl.JobCtrl(workflow=opts.workflow, campaign=opts.campaign)

    jobs = None
    if opts.idle:
        print('Idle jobs for task %s + %s + %s' % (opts.workflow, opts.campaign))
        print(jctrl.getIdle(opts.data_period))

    if opts.running:
        print('Running jobs for task %s + %s + %s' % (opts.workflow, opts.campaign))
        print(jctrl.getRunning(opts.data_period))

    if opts.failed:
        print('Failed jobs for task %s + %s + %s' % (opts.workflow, opts.campaign))
        print(jctrl.getFailed(opts.data_period))

    if opts.done:
        print('Done jobs for task %s + %s + %s' % (opts.workflow, opts.campaign))
        print(jctrl.getDone(opts.data_period))

    if opts.all:
        jobs = jctrl.getJobs(opts.data_period)
        header = 'Jobs status for task: %s + %s + %s' % (opts.workflow, opts.campaign)
        print(header)
        print('='*len(header))
        total = 0
        for status, j in jobs.items():
            text = ' %s (%d):' % (status+' '*(15-len(status)-len(str(len(j)))), len(j))
            print(text, end=' ')
            print(j)
            total += len(j)
        print(' total %s : %d' % (' '*(13-len(str(total))), total))

    if opts.id:
        print('Info for job %s in task %s + %s ' % (str(opts.id), opts.workflow, opts.campaign))
        print(jctrl.getJob(opts.id, opts.data_period))

def wait(opts):
    """
    Wait until all jobs are in status done
    """
    try:
        which_attempt=0
        while opts.howManyAttempts == None or which_attempt<opts.howManyAttempts:
            jctrl = ctrl.JobCtrl(workflow=opts.workflow, campaign=opts.campaign)
            jobs = jctrl.getJobs(opts.data_period)
            prev_failed = set([])
            
            total = len(jobs['idle']+jobs['running']+jobs['failed']+jobs['done'])
            if len(jobs['done']) == total and total != 0:
                print('All jobs in task %s + %s successfully completed' % (opts.workflow, opts.campaign))
                return True
            elif len(jobs['failed']) > 0 and set(jobs['failed']) != set(prev_failed):
                if opts.resubcmd != '':
                    os.system(opts.resubcmd)
                elif opts.jobresubcmd != '':
                    for jid in jobs['failed']:
                        os.system('export JOBID='+str(jid)+'; '+opts.jobresubcmd)
                prev_failed = set(jobs['failed'])
            which_attempt += 1
            time.sleep(opts.sleep)
        return False
    except Exception:
        return False
       
    
def listdsets(opts):
    """
    List all available datasets.
    """

    ctrl.DatasetCtrl.listDatasets()   
        
def getfiles(opts):
    """
    Get files from the specified task.
    """

    dctrl = ctrl.DatasetCtrl(workflow=opts.workflow, campaign=opts.campaign)
    
    if opts.all:
        files = {}
        for afile in dctrl.getAllFiles():
            if afile['type'] in files.keys():
                files[afile['type']].append(afile['path'])
            else:
                files[afile['type']] = [afile['path']]

        if len(files) > 0:
            print('List of files from task %s + %s :' % (opts.workflow, opts.campaign))
            for t, fs in files.items(): 
                print(t+':')
                print('='*len(t+':'))
                for f in fs:
                    print(f)
        else:
            print('No file found in task "%s+%s". Please check if task exist' % (
                opts.campaign, opts.workflow))
            
    if opts.id_range:
        data = dctrl.getFilesIdRange(start_id=opts.id_range[0], n_files=opts.id_range[1], type=opts.type, data=opts.data)        
        for f in data:
            if len(f.keys()) == 1:
                print(list(f.values())[0])
            else:
                print(', '.join(f.values()))
        # check for empty return
        if len(data) == 0:
            print('No file found in task "%s+%s" with %s < ID < %s and type = "%s". Please try with the --all option' % (
                opts.campaign, opts.workflow, opts.id_range[0], opts.id_range[1], opts.type))

def setfile(opts):
    """
    Insert/update file information
    """
    
    dctrl = ctrl.DatasetCtrl(workflow=opts.workflow, campaign=opts.campaign)
    
    if dctrl.insertFile(id=opts.id, path=opts.path, type=opts.type, valid=opts.valid, runs_lumis=opts.runs_lumis, fills=opts.fills):
        print('File information correctly uploaded to the influxdb')
    else:
        print('File information upload to the influxdb FAILED')

### dictionary of available subcommands
subcommands = {
    'exist'     : exist,
    'submit'    : submit,
    'jobctrl'   : jobctrl,
    'status'    : status,
    'wait'      : wait,
    'getfiles'  : getfiles,
    'setfile'   : setfile
}

def cmdline_options():
    """
    Return the argparse instance to handle the cmdline options for this script (needed to easily generate 
    the docs using sphinx-argparse)
    """
    print(os. getcwd())
    ### global options
    parser = argparse.ArgumentParser(description=
    """
    Update task/job information to the ECAL automation influxdb.

    + Example submit (called by submit script):
    ecalautomation.py -c new_campaign -w ECALELF_prod -e 2021A submit -n 100

    + Example job start running (called by job script):     
    ecalautomation.py -c new_campaign -w ECALELF_prod -e 2021A jobctrl --id 4 --running
    """, formatter_class=argparse.RawTextHelpFormatter)
    
    parser.add_argument('-c', '--campaign', dest='campaign', help='ECAL processing campaign', required=True)    
    parser.add_argument('-w', '--workflow', dest='workflow', help='ECAL workflow', required=True)    
    parser.add_argument('--dataPeriod', dest='data_period', default=None, type=int, help='Tag of data period (era/run/fill etc.)') 

    ### sub commands
    subparsers = parser.add_subparsers(dest='subcommand', description='Select the DB operation')

    ### exist subcommand
    exist_parser = subparsers.add_parser('exist', help='Check if task already exist')
    
    ### submit subcommand
    submit_parser = subparsers.add_parser('submit', help='Inject a submitted task into the DB')
    submit_parser.add_argument('--ids', dest='ids', default=None, type=str, 
                               help='List of submitted jobs, if this option is specified option -N is ignored')    
    submit_parser.add_argument('-n', dest='njobs', default=None, type=int,
                               help='Number of submitted jobs, the script will assume that jobs ids span from 0 to N-1') 

    ### job control subcommand
    jobctrl_parser = subparsers.add_parser('jobctrl', help='Update job status in the DB')
    group = jobctrl_parser.add_mutually_exclusive_group(required=True )
    group.add_argument('--idle', action='store_true')
    group.add_argument('--running', action='store_true')
    group.add_argument('--failed', action='store_true')
    group.add_argument('--done', action='store_true')
    jobctrl_parser.add_argument('--id', dest='id', help='Job id within the task', required=True)    
    
    ### status subcommand
    status_parser = subparsers.add_parser('status', help='Jobs status informations from the DB')
    group = status_parser.add_mutually_exclusive_group(required=True )
    group.add_argument('--idle', action='store_true')
    group.add_argument('--running', action='store_true')
    group.add_argument('--failed', action='store_true')
    group.add_argument('--done', action='store_true')
    group.add_argument('--all', action='store_true')
    group.add_argument('--id', dest='id', default=None, help='Job id within the task')    

    ### wait subcommand
    wait_parser = subparsers.add_parser('wait', help='Wait for all jobs in task to be done')
    wait_parser.add_argument('-s', dest='sleep', type=int, default=300, help='Period between checks in seconds') 
    wait_parser.add_argument('--howManyAttempts', dest='howManyAttempts', type=int, default=None, help='Maximum number of tries')      
    wait_parser.add_argument('--resubcmd', dest='resubcmd', type=str, default='', help='Resubmit command (executed once for all failed jobs)')    
    wait_parser.add_argument('--jobresubcmd', dest='jobresubcmd', type=str, default='', help='Resubmit command (executed once for each failed job). The jobid is available in the $JOBID environment variable. Example: `my_resubmit.sh $JOBID`')    

    ### getfiles subcommand
    getfiles_parser = subparsers.add_parser('getfiles', help='Get files for the specified task')
    group = getfiles_parser.add_mutually_exclusive_group(required=True )
    group.add_argument('--all', action='store_true')
    group.add_argument('--id-range', dest='id_range', action=SplitArgs, help='List containing [first_id, n_files]')
    getfiles_parser.add_argument('--type', dest='type', default=None, help='File type')    
    getfiles_parser.add_argument('--data', dest='data', action=SplitArgs, default=['path'], help='File data to be retrived')    

    ### setfile subcommand
    setfile_parser = subparsers.add_parser('setfile', help='Insert/update file info in the influxdb')
    setfile_parser.add_argument('--path', dest='path', default=None, help='File path', required=True)    
    setfile_parser.add_argument('--id', dest='id', default=None, help='Job id within the task', required=True)   
    setfile_parser.add_argument('--type', dest='type', default=None, help='File type')    
    setfile_parser.add_argument('--runs-lumis', dest='runs_lumis', default=None, help='JSON string containing the CMSSW like list of runs and lumis contained in the file')    
    setfile_parser.add_argument('--fills', dest='fills', default=None, action=SplitArgs, help='Comma separated list of fills contained in the file')    
    setfile_parser.add_argument('--valid', dest='valid', default=None, type=bool, help='File integrity has been valided')    
        
   
    return parser

if __name__ == '__main__':
    """
    This script is designed to provide a standard interface to the ECAL automation influxdb.
    """    
    
    opts = cmdline_options().parse_args()

    ret = subcommands[opts.subcommand](opts)

    sys.exit(ret)
