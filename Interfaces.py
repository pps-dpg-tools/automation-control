import subprocess
import os
import controllers.JobCtrl as JobCtrl

def submit_task_to_crab(campaign, workflow, data_period, runs_range, template_path, dataset):
    result = subprocess.run(['python', os.path.dirname(os.path.realpath(__file__))+'/CrabCommandsWrapperScript.py', '--crabCmd', 'submit', '-d', dataset, 
    '-r', runs_range,'-t', template_path, '-c', campaign, 
    '-w', workflow, '--dataPeriod', data_period], stderr=subprocess.STDOUT, env=os.environ)
    return result.returncode


def check_if_crab_task_is_finished(campaign, workflow, data_period):
    result = subprocess.run(['python3', os.path.dirname(os.path.realpath(__file__))+'/AutomationScript.py', '-c', campaign, 
    '-w', workflow, '--dataPeriod', data_period, 'wait', 
    "--resubcmd", "crab resubmit --proxy=/afs/cern.ch/user/e/ecalgit/grid_proxy.x509 crab_wdir", 
    "--howManyAttempts", "1", "-s", "0" ], stderr=subprocess.STDOUT)
    if result.returncode==True:
        jobctrl = JobCtrl(workflow, campaign)
        jobctrl.deleteJobsFromDatabase(data_period)
    return result.returncode