#!/bin/bash

JOBID=${1}
CAMPAIGN=${2}
WORKFLOW=${3}
DATAPERIOD=${4}

set -x

export JOBPYTHONPATH=$PYTHONPATH

export PYTHONPATH=$CMSSW_BASE/external/$SCRAM_ARCH/lib/python3.8/site-packages/
pip3 install --prefix $CMSSW_BASE/external/$SCRAM_ARCH/ -I 'git+https://gitlab.cern.ch/pps-dpg-tools/automation-control.git@feature/no_era_parameter'

AutomationScript.py $CAMPAIGN $WORKFLOW $DATAPERIOD jobctrl  --id $JOBID --running

export PYTHONPATH=$JOBPYTHONPATH
cmsRun -j FrameworkJobReport.xml -p PSet.py
RETCMSSW=$?

export PYTHONPATH=$CMSSW_BASE/external/$SCRAM_ARCH/lib/python3.8/site-packages/

if [ "$RETCMSSW" == "0" ]
then
    AutomationScript.py $CAMPAIGN $WORKFLOW  $DATAPERIOD jobctrl --id $JOBID --done
else
    AutomationScript.py $CAMPAIGN $WORKFLOW  $DATAPERIOD jobctrl --id $JOBID --failed
fi

export PYTHONPATH=JOBPYTHONPATH
exit $RETCMSSW

